<?php
echo 'this is simple string';
echo 'you can also have embeded newlines in string this way as it is okay to do';
//outputs:Arnold once said:"I\'11be back"';
echo 'Arnold once said: "I\.11 be back"';
//outputs:you deleted c:\*.*?
echo 'you deleted c:\*.*';

//outputs:you deleted c:\*.*?
echo 'you deleted c:\*.*';
//outputs:variable do not $expand $either
echo 'variables do not $expand $either';
?>